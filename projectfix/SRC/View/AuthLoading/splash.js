import React, { Component } from 'react';
import { View, ImageBackground, Image } from 'react-native';
import imagelogin from '../../images/imagelogin.png'
import logo from '../../images/logo.png'
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import { MaterialIndicator, } from 'react-native-indicators';
import st_splash from './style'


export default class splash extends React.Component {
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props)
        this.state = {
            login: '',
            indicate: true
        }
    }



    isSignedIn = async () => {
        const isSignedIn = await GoogleSignin.isSignedIn();
        console.log(isSignedIn)
        if (isSignedIn === true) {
            this.props.navigation.navigate('Sucess_google')
        }
        else {
            this.props.navigation.navigate('Login')
        }
    };




    componentDidMount() {
        setTimeout(() => this.isSignedIn(), 5000)
    }


    render() {

        return (
            <ImageBackground source={imagelogin} style={st_splash.imagebackground} >
                <View style={st_splash.container}>
                    <View style={st_splash.logo}>
                        <Image source={logo} />
                    </View>
                    <MaterialIndicator color='red' />
                </View>
            </ImageBackground>
        );
    }
}