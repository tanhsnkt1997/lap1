
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    imagebackground: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(28,117,188,0.9)',
        paddingHorizontal: 25,


    },

    logo: {
        paddingBottom: 10,
        marginTop: 20,
    },


});
