import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Button, Image, Alert, BackHandler } from 'react-native';
import firebase from 'react-native-firebase'
import { GoogleSignin } from 'react-native-google-signin';
import st_sucess_google from './style'


class ActivityIndicatorExample extends Component {
   constructor(props) {
      super(props)
      this.state = {
         userInfo: '',
         test: ''
      }
   }
   componentWillMount() {
      GoogleSignin.configure(
         {
            webClientId: '1031814921418-fhqom15a7btpp79phfri6gs57m9rh731.apps.googleusercontent.com',
         }
      )

   }

   componentDidMount = () => {
      this.getCurrentUser()
   }


   //get user use googlesignin
   getCurrentUser = async () => {
      this.getCurrentUser()
      const { currentUser } = firebase.auth()
      this.setState({ userInfo: currentUser })
   };

   signOut = async () => {
      try {
         await GoogleSignin.revokeAccess();
         await GoogleSignin.signOut();
         this.setState({ user: null }); // Remember to remove the user from your app's state as well
         this.props.navigation.navigate('Login')
      } catch (error) {
         console.error(error);
      }
   };

   Out = () => {
      Alert.alert(
         'Thông báo',
         'Bạn có muốn đăng xuất',
         [

            {
               text: 'Cancel',
               //onPress: () => console.log('Cancel Pressed'),
               style: 'cancel',
            },
            { text: 'OK', onPress: this.signOut }
         ],
         { cancelable: true }
      )
   }




   render() {
      return (
         <View style={st_sucess_google.container}>
            <Image
               style={{ width: 50, height: 50, borderRadius: 50 }}
               source={{ uri: this.state.userInfo.photoURL }}
            />
            <Button title='sign out' onPress={this.Out}></Button>
         </View>
      )
   }
}
export default ActivityIndicatorExample
