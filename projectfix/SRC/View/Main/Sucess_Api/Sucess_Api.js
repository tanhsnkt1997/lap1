import React, { Component } from 'react';
import { Text, View, Button, BackHandler, Alert } from 'react-native';
import st from './style'

export default class Sucess_Api extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user: '',
            pass: '',
            repass: '',
            login: true,
        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.backPressed);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    backPressed = () => {
        Alert.alert(
            'Exit App',
            'Do you want to exit?',
            [
                { text: 'No', style: 'cancel' },
                { text: 'Yes', onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false });
        return true;
    }


    render() {
        const { navigation } = this.props
        const itemId = navigation.getParam('userData');
        const name = itemId.data.result.username
        return (
            <View style={st.container}>
                <Text>Xin chào : {name}</Text>
                <Button title='Đăng xuất' />
            </View>

        );
    }
}