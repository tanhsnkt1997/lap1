import axios from 'axios/index';
const axiosFetch = (data) => (
    axios.post('https://test01.e300.vn/web/session/authenticate', data, {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    })
)
export default axiosFetch
