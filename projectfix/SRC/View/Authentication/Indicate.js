import st_login from './Login/Style/st_login_portrait'
import imagelogin from '../../images/imagelogin.png'
import logo from '../../images/logo.png'
const indicator = () => (
    <ImageBackground source={imagelogin} style={st_login.imagebackground} >
        <View style={st_login.container}>
            <View style={st_login.logo}>
                <Image source={logo} />
            </View>
            <MaterialIndicator color='red' />
        </View>
    </ImageBackground>
)

export default indicator