import React, { Component } from 'react';
import { View, ImageBackground, Image, TextInput, Text, Button, TouchableOpacity, StyleSheet, Dimensions, ActivityIndicator, Alert } from 'react-native';
import imagelogin from '../../../images/imagelogin.png'
import logo from '../../../images/logo.png'
import st_login from './Style/st_login_portrait'
import firebase from 'react-native-firebase'
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import { MaterialIndicator } from 'react-native-indicators';
import axiosFetch from '../config/axios'
import ListTask from '../Login/render_login'
import google_config from '../config/googleConfig'
import reder_indicater from '../Indicate'


export default class Login extends React.Component {
  constructor(props) {
    super(props)
    const isPortrait = () => {
      const dim = Dimensions.get('screen');
      return dim.height >= dim.width;
    };

    //state
    this.state = {
      user: '',
      pass: '',
      datas: '',
      userEmail: '',     
      hidePass: true,
      validate: true,
      checknull: false,
      checkpass: false,
      indicate: false,
      isLoginScreenPresented: '',
      orientation: isPortrait() ? 'portrait' : 'landscape'

    }
    // Event Listener for orientation changes
    Dimensions.addEventListener('change', () => {
      this.setState({
        orientation: isPortrait() ? 'portrait' : 'landscape'
      });
    });
  }
  static navigationOptions = {
    header: null,
  };



  //Check validate
  test = (text, type) => {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (type === 'username') {
      this.setState({ user: text })
      //console.log(this.state.user)
      if (re.test(text)) {
        this.setState({ validate: true })
      }
      else {
        this.setState({ validate: false })
      }
    }
    if (type == "password") {
      this.setState({ pass: text })
    }
    this.setState({ checknull: false, checkpass: false })
  }


  //function Signin user and pass
  SignIn = () => {
    if (this.state.user !== '' && this.state.pass !== '') {
      if (!this.state.validate) {

      } else {
        let data = JSON.stringify({
          params: {
            db: "hhd_e300_vn_test01",
            login: this.state.user,
            password: this.state.pass
          }
        })

        axiosFetch(data)
          .then(response => {
            if ("error" in response.data) {
              Alert.alert("Lỗi", "Đăng nhập thất bại.\nVui lòng kiểm tra lại thông tin");
            } else {
              if ("result" in response.data) {
                this.setState({ indicate: true })
                this.props.navigation.navigate("Sucess_Api", { userData: response });
              }
            }
          })
          .catch(err => console.log(err))
      }
    }
    else {
      // this.setState({ checknull: true })
      if (this.state.user === '') {
        this.setState({ checknull: true })
      }
      if (this.state.pass === '') {
        this.setState({ checkpass: true })
      }
    }
  }

  //function google sigin
  googleLogin = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      google_config()
      const data = await GoogleSignin.signIn();
      this.setState({ indicate: true })
      // create a new firebase credential with the token
      const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken)
      await firebase.auth().signInWithCredential(credential)
        .then(() => this.props.navigation.navigate('Sucess_google'))
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
      }
      else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      }
    }
  }

  //Hide and show password
  hideshow = () => {
    this.setState({ hidePass: !this.state.hidePass })
  }


  render() {
    const { validate, orientation, checknull, hidePass, checkpass, isSigninInProgress } = this.state
    return this.state.indicate === true ? reder_indicater() : (
      <ListTask
        updateinput={(text, type) => this.test(text, type)}
        orientation={orientation}
        validate={validate}
        checknull={checknull}
        hidePass={hidePass}
        checkPass={checkpass}
        hideshow={this.hideshow}
        SignIn={this.SignIn}
        googleLogin={this.googleLogin}
        isSigninInProgress={isSigninInProgress}
      />
    );
  }
}
