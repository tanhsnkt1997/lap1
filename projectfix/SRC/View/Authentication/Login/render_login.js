import React, { Component } from 'react';
import { View, ImageBackground, Image, TextInput, Text, Button, TouchableOpacity, StyleSheet, Dimensions, ActivityIndicator, Alert } from 'react-native';
import imagelogin from '../../../images/imagelogin.png'
import logo from '../../../images/logo.png'
import hidepass from '../../../icons/hide.png'
import showpass from '../../../icons/show.png'
import st_login from './Style/st_login_portrait'
import { GoogleSigninButton } from 'react-native-google-signin';
import st_login2 from './Style/st_login_landscape'


const Render_Listtask = ({
    updateinput = () => { },
    orientation = '',
    validate = '',
    checknull = '',
    hidePass = '',
    hideshow = () => { },
    checkPass = '',
    SignIn = () => { },
    googleLogin = () => { },

}) => {
    return (
        <ImageBackground source={imagelogin} style={orientation === 'portrait' ? st_login.imagebackground : st_login2.imagebackground} >
            <View style={orientation === 'portrait' ? st_login.container : st_login2.container}>
                <View style={orientation === 'portrait' ? st_login.logo : st_login2.logo}>
                    <Image source={logo} />
                </View>

                <View style={orientation === 'portrait' ? st_login.textBoxBtnHolder : st_login2.textBoxBtnHolder}>
                    <TextInput underlineColorAndroid="transparent"
                        style={validate === true ? st_login.textBox : st_login.textBoxErr}
                        placeholder='Username'
                        placeholderTextColor={'#fff'}
                        onChangeText={(user) => updateinput(user, "username")} />
                    {validate === true ? null : <Text style={st_login.validate}>Vui lòng kiểm tra lại định dạng email</Text>}
                    {checknull === false ? null : <Text style={st_login.validate}>Trường này không được để trống</Text>}
                </View>

                <View style={orientation === 'portrait' ? st_login.textBoxBtnHolder : st_login2.textBoxBtnHolder}>
                    <TextInput underlineColorAndroid="transparent"
                        secureTextEntry={hidePass}
                        style={orientation === 'portrait' ? st_login.textBox : st_login2.textBox}
                        placeholder='Password'
                        placeholderTextColor={'#fff'}
                        onChangeText={(pass) => updateinput(pass, "password")} />

                    <TouchableOpacity activeOpacity={0.8}
                        style={orientation === 'portrait' ? st_login.visibilityBtn : st_login2.visibilityBtn}
                        onPress={() => hideshow()}>
                        <Image source={hidePass ? hidepass : showpass}
                            style={orientation === 'portrait' ? st_login.btnImage : st_login2.btnImage} />
                    </TouchableOpacity>
                    {checkPass === true ? <Text style={st_login.validate}>Trường này không được để trống</Text> : null}
                </View>

                <View style={orientation === 'portrait' ? st_login.btnlogin : st_login2.btnlogin}>
                    <TouchableOpacity style={orientation === 'portrait' ? st_login.Button : st_login2.Button}
                        onPress={() => SignIn()}>
                        <Text>Login</Text>
                    </TouchableOpacity>

                    <GoogleSigninButton
                        style={orientation === 'portrait' ? st_login.ButtonGoogle : st_login2.ButtonGoogle}
                        size={GoogleSigninButton.Size.Wide}
                        color={GoogleSigninButton.Color.Light}
                        onPress={() => googleLogin()}
                    />
                </View>
            </View>
        </ImageBackground>
    )
}

export default Render_Listtask;