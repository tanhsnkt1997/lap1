import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    textinput:
    {
        position: 'relative',
        alignSelf: 'stretch',
        justifyContent: 'center',
        flex: 1,
    },

    childtext_input: {
        flex: 1
    }
    ,
    imagebackground: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(28,117,188,0.9)',
        paddingHorizontal: 25,


    },
    boxbtn: {
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        //flex:1,
        paddingBottom: 10,
      //  marginBottom: 20,
        marginTop: 20,
    },
    text_input: {
        width: '60%',
        height: 45,
        color: 'rgba(255,255,255,0.7)',
        borderBottomWidth: 1,
        marginHorizontal: 25,
        marginBottom: 10,
        borderBottomColor: '#fff',


    },
    text_in_input: {
        position: 'absolute',
        right: 10,
        height: 40,
        width: 35,
        top: 5
        //  padding: 10
    },
    btnImage:
    {
        resizeMode: 'contain',
        height: '200%',
        width: '200%'
    },

    textBoxBtnHolder:
    {
        position: 'relative',
        alignSelf: 'stretch',
        justifyContent: 'center',


    },


    textBox:
    {
        fontSize: 18,
        alignSelf: 'stretch',
        height: 45,
        paddingRight: 45,
        paddingLeft: 8,
        borderBottomWidth: 1,
        paddingVertical: 0,
        borderColor: '#fff',
        borderRadius: 5,
        color: '#fff'
        // width:width-40

    },
    textBoxErr:
    {
        fontSize: 18,
        alignSelf: 'stretch',
        height: 45,
        paddingRight: 45,
        paddingLeft: 8,
        borderBottomWidth: 1,
        paddingVertical: 0,
        borderColor: 'red',
        borderRadius: 5,
        color: '#fff'
        // width:width-40

    },

    visibilityBtn:
    {
        position: 'absolute',
        right: 3,
        height: 44,
        width: 39,
        padding: 5
    },

    btnImage:
    {
        resizeMode: 'contain',
        height: '100%',
        width: '100%'
    },
    validate: {
        color: 'red'
    },
    btnlogin: {
        paddingBottom:20,
        justifyContent: 'center',
        width: '100%',
        alignItems: 'center',
    },

    Button: {
        width: '60%',
        height: 50, marginTop: 10, backgroundColor: 'rgb(156, 230, 240)', justifyContent: 'center', alignItems: 'center'
    },
    ButtonGoogle: {
        width: '61%',
        height: 60 , marginTop: 10, justifyContent: 'center', alignItems: 'center'
    },

});
