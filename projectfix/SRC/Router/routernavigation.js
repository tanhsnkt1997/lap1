import Login from '../View/Authentication/Login/Login'
import Sucess_google from '../View/Main/Sucess_Google/Sucess_google'
import Sucess_Api from '../View/Main/Sucess_Api/Sucess_Api'
import splash from '../View/AuthLoading/splash'
import { createStackNavigator, createAppContainer, createBottomTabNavigator, createSwitchNavigator } from "react-navigation";

//Creat stack

const Switch = createSwitchNavigator({
    splash: splash,
    Login: Login,
    Sucess_google: Sucess_google,
    Sucess_Api: Sucess_Api,
})


export default createAppContainer(Switch)


